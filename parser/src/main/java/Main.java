import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;

public class Main {

    public static void main(String[] args) {

        HashMap<Integer, Paragem> paragens = new HashMap<Integer, Paragem>();

        String csvParagens = "/home/pedro/Code/srcr/srcr-tp2/resources/paragem.csv";
        String csvAdjacencias = "/home/pedro/Code/srcr/srcr-tp2/resources/adjacencias.csv";

        BufferedReader br = null;
        String line = "";
        String splitter = ";";

        int j = 0;
        try {

                br = new BufferedReader(new FileReader(csvParagens));

                while ((line = br.readLine()) != null) {

                    try {

                        ArrayList<Integer> carreiras = new ArrayList<>();
                        String arrSpliter = ",";
                        String[] paragem = line.split(splitter);
                        String[] carrs = paragem[7].split(arrSpliter);
                        for(int i = 0; i < carrs.length; i++)
                            carreiras.add(Integer.parseInt(carrs[i]));

                        Paragem p = new Paragem(
                                Integer.parseInt(paragem[0]),
                                Float.parseFloat(paragem[1]),
                                Float.parseFloat(paragem[2]),
                                paragem[3],
                                paragem[4],
                                paragem[5],
                                paragem[6],
                                carreiras,
                                Integer.parseInt(paragem[8]),
                                paragem[9],
                                paragem[10]
                        );

                        paragens.put(p.getGid(), p);
                        j++;
                        //System.out.println(paragem);
                    } catch (Exception e) {
                        e.printStackTrace();
                        System.out.println(j + "\n");
                    }
                }

        } catch (Exception e) {
            e.printStackTrace();
        }

        //for(Integer key : paragens.keySet())
            //System.out.println(paragens.get(key).toString());
    }


}

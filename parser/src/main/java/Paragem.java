import java.util.ArrayList;

public class Paragem {

    private int gid;
    private float latitude;
    private float longitude;
    private String estadoConservacao;
    private String tipoAbrigo;
    private String publicidade;
    private String operadora;
    private ArrayList<Integer> carreira;
    private int codigoRua;
    private String nomeRua;
    private String freguesia;

    public Paragem(int gid, float latitude, float longitude,
                   String estadoConservacao, String tipoAbrigo,
                   String publicidade, String operadora, ArrayList<Integer> carreira, int codigoRua,
                   String nomeRua, String freguesia) {
        this.gid = gid;
        this.latitude = latitude;
        this.longitude = longitude;
        this.estadoConservacao = estadoConservacao;
        this.tipoAbrigo = tipoAbrigo;
        this.publicidade = publicidade;
        this.operadora = operadora;
        this.carreira = carreira;
        this.codigoRua = codigoRua;
        this.nomeRua = nomeRua;
        this.freguesia = freguesia;
    }

    public int getGid() {
        return gid;
    }

    public void setGid(int gid) {
        this.gid = gid;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public String getEstadoConservacao() {
        return estadoConservacao;
    }

    public void setEstadoConservacao(String estadoConservacao) {
        this.estadoConservacao = estadoConservacao;
    }

    public String getTipoAbrigo() {
        return tipoAbrigo;
    }

    public void setTipoAbrigo(String tipoAbrigo) {
        this.tipoAbrigo = tipoAbrigo;
    }

    public String getPublicidade() {
        return publicidade;
    }

    public void setPublicidade(String publicidade) {
        this.publicidade = publicidade;
    }

    public ArrayList<Integer> getCarreira() {
        return carreira;
    }

    public void setCarreira(ArrayList<Integer> carreira) {
        this.carreira = carreira;
    }

    public int getCodigoRua() {
        return codigoRua;
    }

    public void setCodigoRua(int codigoRua) {
        this.codigoRua = codigoRua;
    }

    public String getNomeRua() {
        return nomeRua;
    }

    public void setNomeRua(String nomeRua) {
        this.nomeRua = nomeRua;
    }

    public String getFreguesia() {
        return freguesia;
    }

    public void setFreguesia(String freguesia) {
        this.freguesia = freguesia;
    }

    @Override
    public String toString() {
        return "Paragem{" +
                "gid=" + gid +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", estadoConservacao='" + estadoConservacao + '\'' +
                ", tipoAbrigo='" + tipoAbrigo + '\'' +
                ", publicidade='" + publicidade + '\'' +
                ", operadora='" + operadora + '\'' +
                ", carreira=" + carreira +
                ", codigoRua=" + codigoRua +
                ", nomeRua='" + nomeRua + '\'' +
                ", freguesia='" + freguesia + '\'' +
                '}';
    }
}

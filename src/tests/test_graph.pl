% vim: syntax=prolog

%---------------------------------------------------------------------------------------------------
% SICStus PROLOG: Declarações iniciais -------------------------------------------------------------
%---------------------------------------------------------------------------------------------------

:- set_prolog_flag( discontiguous_warnings,off ).
:- set_prolog_flag( single_var_warnings,off    ).
:- set_prolog_flag( unknown,fail               ).
:- op( 900,xfy,'::' ).

%---------------------------------------------------------------------------------------------------
% SMALLER DATA SET ---------------------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------

adjacencia(0, 1, 1, -1).
adjacencia(0, 2, 2, -1).
adjacencia(0, 3, 5, -1).
adjacencia(1, 4, 4, -1).
adjacencia(1, 5, 9, -1).
adjacencia(2, 5, 5, -1).
adjacencia(2, 6, 16, -1).
adjacencia(3, 6, 2, -1).
adjacencia(4, 7, 18, -1).
adjacencia(5, 7, 13, -1).
adjacencia(6, 7, 2, -1).
adjacencia(4, 8, 3, -1).
adjacencia(4, 9, ,4 -1).
adjacencia(7, 10, 2, -1).
adjacencia(7, 12, 4, -1).
adjacencia(8, 15, 6, -1).
adjacencia(9, 13, 3, -1).
adjacencia(10, 13, 2, -1).
adjacencia(13, 15, 2, -1).
adjacencia(10, 15, 3, -1).
adjacencia(12, 15, 4, -1).
% vim: syntax = prolog

%---------------------------------------------------------------------------------------------------
% SICStus PROLOG: Declarações iniciais -------------------------------------------------------------
%---------------------------------------------------------------------------------------------------

:- set_prolog_flag( discontiguous_warnings,off ).
:- set_prolog_flag( single_var_warnings,off    ).
:- set_prolog_flag( unknown,fail               ).
:- op( 900,xfy,'::' ).

%---------------------------------------------------------------------------------------------------
% FILTERS ------------------------------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------

none(Paragem, FurtherArgs).

operator(Paragem, Operadoras) :-
    paragem(Paragem, _, _, _, _, _, Operadora, _, _, _, _),
    member(Operadora, Operadoras).

not_operator(Paragem, Operadoras) :-
    paragem(Paragem, _, _, _, _, _, Operadora, _, _, _, _),
    \+ member(Operadora, Operadoras).

advertised(Paragem, FurtherArgs) :-
    paragem(Paragem, _, _, _, _, 'Yes', _, _, _, _, _).

sheltered(Paragem, FurtherArgs) :-
    \+ paragem(Paragem, _, _, _, 'Sem Abrigo', _, _, _, _, _, _).

carrierCount(Paragem, CarrierCount) :-
    findall(Carreira, adjacencia(Paragem, _, _, Carreira), S),
    length(S, CarrierCount).

bindCarrierCount([], Result) :-
    Result = [].
bindCarrierCount([First|Path], Result) :-
    carrierCount(First, Count),
    bindCarrierCount(Path, NewResult),
    Result = [Count/First|NewResult].

getMaxCarrierCount(Lista, Result) :-
    sort(Lista, SortedList),
    reverse(SortedList, Result).

getFrontRunner([Count/Paragem|Tail], Result) :-
    Result = Count.

filterByMaxCarrier(MaxCarrier, [], Result) :-
    Result = [].
filterByMaxCarrier(MaxCarrier, [Count/Paragem|Tail], Result) :-
    (Count == MaxCarrier,
    filterByMaxCarrier(MaxCarrier, Tail, NewResult),
    Result = [Paragem/Count|NewResult]);
    (filterByMaxCarrier(MaxCarrier, Tail, NewResult),
    Result = NewResult).

pruneLast([X|Xs], Ys) :-                 
   pruneLastAux(Xs, Ys, X).           

pruneLastAux([], [], _).
pruneLastAux([X1|Xs], [X0|Ys], X0) :-  
   pruneLastAux(Xs, Ys, X1).  

pathBuilder([Origin], Algorithm, Result) :-
    Result = [].
pathBuilder([Origin, Next|Rest], Algorithm, Result) :-
    call(Algorithm, Origin, Next, R, 'none', undefined),
    pruneLast(R, Pruned),
    pathBuilder([Next|Rest], Algorithm, NewResult),
    Result = [Pruned|NewResult].

myFlatten([], []) :- !.
myFlatten([L|Ls], FlatL) :-
    !,
    myFlatten(L, NewL),
    myFlatten(Ls, NewLs),
    append(NewL, NewLs, FlatL).
myFlatten(L, [L]).

lastInList([X], Res) :-
    Res is X.
lastInList([First|Tail], Res) :-
    lastInList(Tail, Res).

insertTail([], N, [N]).
insertTail([H|T], N, [H|R]) :- insertTail(T, N, R).

actualPathBuilder(NodeList, Algorithm, Result) :-
    pathBuilder(NodeList, Algorithm, PreRes),
    myFlatten(PreRes, FlattenedRes),
    lastInList(NodeList, Last),
    insertTail(FlattenedRes, Last, Result).
% vim: syntax=prolog

%---------------------------------------------------------------------------------------------------
% INCLUDES -----------------------------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------

:- include('filters.pl').
:- include('maths.pl').
:- include('print_utils.pl').

%---------------------------------------------------------------------------------------------------
% SICStus PROLOG: Declarações iniciais -------------------------------------------------------------
%---------------------------------------------------------------------------------------------------

:- set_prolog_flag( discontiguous_warnings,off ).
:- set_prolog_flag( single_var_warnings,off    ).
:- set_prolog_flag( unknown,fail               ).
:- op( 900,xfy,'::' ).
:- use_module(library(lists)).
:- use_module(library(system)).

%---------------------------------------------------------------------------------------------------
% DEPTH FIRST SEARCH ALGORITHM ---------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------

df_solver(Origem, Destino, Solution, Filter, Arguments) :-
    depth_first_engine([[Origem]], Destino, RevertedSolution, Filter, Arguments),
    reverse(RevertedSolution, Solution).

possible_nextnodes_df([Node|Path], NewPaths, Filter, Arguments) :-
    findall([NewNode,Node|Path],
            (
                adjacencia(Node, NewNode, _, _),
                call(Filter, NewNode, Arguments),
                \+ member(NewNode, Path)
            ),
        NewPaths).


depth_first_engine([[Goal|Path]|_], Goal, [Goal|Path], Filter, Arguments).
depth_first_engine([Path|Queue], Goal, FinalPath, Filter, Arguments) :-
    possible_nextnodes_df(Path, NewPaths, Filter, Arguments),
    append(NewPaths, Queue, NewQueue),
    depth_first_engine(NewQueue, Goal, FinalPath, Filter, Arguments).

%---------------------------------------------------------------------------------------------------
% QUERY 1 ------------------------------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------

query1depthfirst(Origem, Destino) :-
    statistics(walltime, [_|T1]),
    df_solver(Origem, Destino, R, 'none', undefined),
    statistics(walltime, [_|T2]),
    prettify(R),
    pathCost(R, Custo),
    printCost(Custo),
    printTime(T2).

%---------------------------------------------------------------------------------------------------
% QUERY 2 ------------------------------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------

query2depthfirst(Origem, Destino, Operadoras) :-
    statistics(walltime, [_|T1]),
    df_solver(Origem, Destino, R, 'operator', Operadoras),
    statistics(walltime, [_|T2]),
    prettify(R),
    pathCost(R, Custo),
    printCost(Custo),
    printTime(T2).

%---------------------------------------------------------------------------------------------------
% QUERY 3 ------------------------------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------

query3depthfirst(Origem, Destino, Operadoras) :-
    statistics(walltime, [_|T1]),
    df_solver(Origem, Destino, R, 'not_operator', Operadoras),
    statistics(walltime, [_|T2]),
    prettify(R),
    pathCost(R, Custo),
    printCost(Custo),
    printTime(T2).

%---------------------------------------------------------------------------------------------------
% QUERY 4 ------------------------------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------

query4depthfirst(Origem, Destino) :-
   statistics(walltime, [_|T1]),
   df_solver(Origem, Destino, R, 'none', undefined),
   bindCarrierCount(R, BindedCountList),
   pathCost(R, Custo),
   getMaxCarrierCount(BindedCountList, SortedCarrierList),
   getFrontRunner(SortedCarrierList, MaxCarrier),
   filterByMaxCarrier(MaxCarrier, BindedCountList, Result),
   statistics(walltime, [_|T2]),
   prettifyPairList(Result),
   printCost(Custo),
   printTime(T2).

%---------------------------------------------------------------------------------------------------
% QUERY 5 ------------------------------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------

% O método de resolução desta query é utilizar Breadth-First.

%---------------------------------------------------------------------------------------------------
% QUERY 6 ------------------------------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------

% Não tem equivalente -> Muito melhor com pesquisa informaada.

%---------------------------------------------------------------------------------------------------
% QUERY 7 ------------------------------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------

query7depthfirst(Origem, Destino) :-
   statistics(walltime, [_|T1]),
   advertised(Origem, undefined),
   advertised(Destino, undefined),
   df_solver(Origem, Destino, R, 'advertised', undefined),
   statistics(walltime, [_|T2]),
   prettify(R),
   pathCost(R, Custo),
   printCost(Custo),
   printTime(T2).

%---------------------------------------------------------------------------------------------------
% QUERY 8 ------------------------------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------

query8depthfirst(Origem, Destino) :-
   statistics(walltime, [_|T1]),
   sheltered(Origem, undefined),
   sheltered(Destino, undefined),
   df_solver(Origem, Destino, R, 'sheltered', undefined),
   statistics(walltime, [_|T2]),
   prettify(R),
   pathCost(R, Custo),
   printCost(Custo),
   printTime(T2).

%---------------------------------------------------------------------------------------------------
% QUERY 9 ------------------------------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------

query9depthfirst(NodeList) :-
   statistics(walltime, [_|T1]),
   actualPathBuilder(NodeList, 'df_solver', Result),
   statistics(walltime, [_|T2]),
   prettify(Result),
   pathCost(Result, Cost),
   printCost(Cost),
   printTime(T2).
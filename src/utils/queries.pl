% vim: syntax = prolog

%---------------------------------------------------------------------------------------------------
% INCLUDES -----------------------------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------

:- include('../kbs/adjacencias.pl').
:- include('../kbs/paragens.pl').
:- include('depthfirst_search.pl').
:- include('breadthfirst_search.pl').
:- include('greedy_search.pl').
:- include('astar_search.pl').

%------------------------------------------------------------------------------
% SICStus PROLOG: Declarações iniciais ---------------------------------------
%------------------------------------------------------------------------------

:- set_prolog_flag( discontiguous_warnings,off ).
:- set_prolog_flag( single_var_warnings,off    ).
:- set_prolog_flag( unknown,fail               ).
:- op( 900,xfy,'::' ).
% vim: syntax=prolog

%---------------------------------------------------------------------------------------------------
% INCLUDES -----------------------------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------

:- include('filters.pl').
:- include('maths.pl').
:- include('print_utils.pl').

%---------------------------------------------------------------------------------------------------
% SICStus PROLOG: Declarações iniciais -------------------------------------------------------------
%---------------------------------------------------------------------------------------------------

:- set_prolog_flag( discontiguous_warnings,off ).
:- set_prolog_flag( single_var_warnings,off    ).
:- set_prolog_flag( unknown,fail               ).
:- op( 900,xfy,'::' ).
:- use_module(library(lists)).
:- use_module(library(system)).

%---------------------------------------------------------------------------------------------------
% A* SEARCH ALGORITHM ------------------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------

chooseNodeAStar(Node/From/Cost, [Node/FromStop/NodeCost|Tail], Seen, Result) :-
    Cost < NodeCost,
    TempResult = [Node/From/Cost|Tail],
    append(TempResult, Seen, Result);
    TempResult = [Node/FromStop/NodeCost|Tail],
    append(TempResult, Seen, Result).

updateVisitedAStar(CurrStop/From/Cost, [], Seen, Result):-
   Result = [CurrStop/From/Cost|Seen].
updateVisitedAStar(CurrStop/From/Cost, [Node/FromStop/NodeCost|T], Seen, Result) :-
    chooseNodeAStar(CurrStop/From/Cost, [Node/FromStop/NodeCost|T], Seen, Result).
updateVisitedAStar(CurrStop/From/Cost, [Stop/FromStop/NodeCost|T], Seen, Result) :-
   updateVisitedAStar(CurrStop/From/Cost, T, [Stop/FromStop/NodeCost|Seen], Result).

buildPathFromHistory(From, Dest, Visited, Result) :-
   buildPathFromHistoryAux(Visited,From,Dest,Visited,Result).

buildPathFromHistoryAux([Dest/From/_|_], From, Dest, _, Result) :-
   Result = [Dest,From].
buildPathFromHistoryAux([Dest/FromNode/_|T], From, Dest, Visited, [Dest|Result]) :-
   buildPathFromHistoryAux(Visited, From, FromNode, Visited, Result).
buildPathFromHistoryAux([H|T], From, Dest, Visited, Result) :-
   buildPathFromHistoryAux(T, From, Dest, Visited, Result).


astar_solver(From, From, Path, Filter, FurtherArgs) :-
    R = [From],!.
astar_solver(From, Dest, Path, Filter, FurtherArgs) :-
    astar_engine(From/0, undefined, Dest, [], [], ToExtract, Filter, FurtherArgs),
    buildPathFromHistory(From, Dest, ToExtract, ReversedPath),
    reverse(ReversedPath, Path),
    !.

astar_engine(Dest/Cost, From, Dest, Visited, Expanded, History, Filter, FurtherArgs) :-
    History = [Dest/From/Cost|Visited], !.
astar_engine(Node/Cost, From, Dest, Visited, Expanded, History, Filter, FurtherArgs) :-
    updateFringeAStar(Node, Cost, Dest, Expanded, ToExpand, Filter, FurtherArgs, ExpansionResult),
    chooseNextNodeAStar(Node, ExpansionResult, Visited, [CostToDest/NextNode/FromNode/CostToNode|NextIterExpanded]),
    updateVisitedAStar(Node/From/Cost, Visited, [], UpdatedVisited),
    astar_engine(NextNode/CostToNode, FromNode, Dest, UpdatedVisited, NextIterExpanded, History, Filter, FurtherArgs).

chooseNextNodeAStar(CurrentNode, [CostToDest/Node/FromNode/CostToNode|T],Visited,R) :-
    \+ isFringeNodeAStar(Node,Visited),
    CurretNode = FromNode,
    R = [CostToDest/Node/FromNode/CostToNode|T];
    chooseNextAStar(CurrentNode, T, Visited, R).

updateFringeAStar(Node, Cost, Dest, Expanded, ToExpand, Filter, FurtherArgs, R) :-
    expandFringeAStar(Node, Cost, Dest, ToExpand, Filter, FurtherArgs),
    append(ToExpand, Expanded, ToSort),
    sort(ToSort, R).

expandFringeAStar(From, TotalCost, Dest, ExpansionResult, Filter, FurtherArgs) :-
    getPossibleExpansionsAStar(From, TotalCost, Dest, [], Unsorted, Filter, FurtherArgs),
    sort(Unsorted, ExpansionResult),!.
getPossibleExpansionsAStar(From, TotalCost, Dest, Visited, R, Filter, FurtherArgs) :-
    call(Filter, Node, FurtherArgs),
    adjacencia(From,Node,Cost,_),
    TransitionCost is (TotalCost + Cost),
    estimate(Node,Dest,DistToDest),
    CostToDest is (TotalCost + Cost + DistToDest),
    \+ member(CostToDest/Node/From/TransitionCost, Visited),
    getPossibleExpansionsAStar(From, TotalCost, Dest, [CostToDest/Node/From/TransitionCost|Visited],R, Filter, FurtherArgs),!;
    R = Visited,!.


isFringeNodeAStar(Orig, []):-
   fail,!.
isFringeNodeAStar(Node, [CostToDest/Origin/Node/Cost|T]).
isFringeNodeAStar(CurrStop, [CostToDest/Origin/Node/Cost|T]) :-
   CurrStop =\= Node,
   isFringeNodeAStar(CurrStop, T).

%---------------------------------------------------------------------------------------------------
% QUERY 1 ------------------------------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------

query1astar(Origem, Destino) :-
    statistics(walltime, [_|T1]),
    astar_solver(Origem, Destino, R, 'none', undefined),
    pathCost(R, Custo),
    statistics(walltime, [_|T2]),
    prettify(R),
    printCost(Custo),
    printTime(T2).

%---------------------------------------------------------------------------------------------------
% QUERY 2 ------------------------------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------

query2astar(Origem, Destino, Operadoras) :-
   statistics(walltime, [_|T1]),
   astar_solver(Origem, Destino, R, 'operator', Operadoras),
   pathCost(R, Custo),
   statistics(walltime, [_|T2]),
   prettify(R),
   printCost(Custo),
   printTime(T2).

%---------------------------------------------------------------------------------------------------
% QUERY 3 ------------------------------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------

query3astar(Origem, Destino, Operadoras) :-
   statistics(walltime, [_|T1]),
   astar_solver(Origem, Destino, R, 'not_operator', Operadoras),
   pathCost(R, Custo),
   statistics(walltime, [_|T2]),
   prettify(R),
   printCost(Custo),
   printTime(T2).

%---------------------------------------------------------------------------------------------------
% QUERY 4 ------------------------------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------

query4astar(Origem, Destino) :-
   statistics(walltime, [_|T1]),
   astar_solver(Origem, Destino, R, 'none', undefined),
   pathCost(R, Custo),
   bindCarrierCount(R, BindedCountList),
   getMaxCarrierCount(BindedCountList, SortedCarrierList),
   getFrontRunner(SortedCarrierList, MaxCarrier),
   filterByMaxCarrier(MaxCarrier, BindedCountList, Result),
   statistics(walltime, [_|T2]),
   prettifyPairList(Result),
   printCost(Custo),
   printTime(T2).

%---------------------------------------------------------------------------------------------------
% QUERY 5 ------------------------------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------

% O método de resolucao desta query é o algoritmo Breadth-First.

%---------------------------------------------------------------------------------------------------
% QUERY 6 ------------------------------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------

query6astar(Origem, Destino) :-
    statistics(walltime, [_|T1]),
    astar_solver(Origem, Destino, R, 'none', undefined),
    pathCost(R, Custo),
    statistics(walltime, [_|T2]),
    prettify(R),
    printCost(Custo),
    printTime(T2).

%---------------------------------------------------------------------------------------------------
% QUERY 7 ------------------------------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------

query7astar(Origem, Destino) :-
   statistics(walltime, [_|T1]),
   advertised(Origem, undefined),
   advertised(Destino, undefined),
   astar_solver(Origem, Destino, R, 'advertised', undefined),
   pathCost(R, Custo),
   statistics(walltime, [_|T2]),
   prettify(R),
   printCost(Custo),
   printTime(T2).

%---------------------------------------------------------------------------------------------------
% QUERY 8 ------------------------------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------

query8astar(Origem, Destino) :-
   statistics(walltime, [_|T1]),
   sheltered(Origem, undefined),
   sheltered(Destino, undefined),
   astar_solver(Origem, Destino, R, 'sheltered', undefined),
   pathCost(R, Custo),
   statistics(walltime, [_|T2]),
   prettify(R),
   printCost(Custo),
   printTime(T2).

%---------------------------------------------------------------------------------------------------
% QUERY 9 ------------------------------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------

query9astar(NodeList) :-
   statistics(walltime, [_|T1]),
   actualPathBuilder(NodeList, 'astar_solver', Result),
   statistics(walltime, [_|T2]),
   prettify(Result),
   pathCost(Result, Cost),
   printCost(Cost),
   printTime(T2).
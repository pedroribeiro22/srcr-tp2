%---------------------------------------------------------------------------------------------------
% SICStus PROLOG: Declarações iniciais -------------------------------------------------------------
%---------------------------------------------------------------------------------------------------

:- set_prolog_flag( discontiguous_warnings,off ).
:- set_prolog_flag( single_var_warnings,off    ).
:- set_prolog_flag( unknown,fail               ).
:- op( 900,xfy,'::' ).

%---------------------------------------------------------------------------------------------------
% PRETTY PRINTS ------------------------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------

prettify(R) :-
    write('\n\n'),
    write(R),
    write('\n\n').

prettifyPairList([]).
prettifyPairList([Paragem/Count|Tail]) :-
    write('PARAGEM [gid]: '),
    write(Paragem),
    write(' --->  '),
    write(Count),
    write(' carreiras.\n'),
    prettifyPairList(Tail).

printTime(Time) :-
    write('This query took '),
    write(Time),
    write(' milliseconds.\n\n').

printCost(Cost) :-
    write('Cost: '),
    write(Cost),
    write(' time units.\n\n').


list_sum([Item], Item).
list_sum([Item1,Item2 | Tail], Total) :-
    NewValue is (Item1 + Item2),
    list_sum([NewValue|Tail], Total).

auxPathCost([Elem], Result) :-
    Result = [].
auxPathCost([First, Second|Path], Result) :-
    adjacencia(First, Second, Cost, _),
    auxPathCost([Second|Path], NewResult),
    Result = [Cost|NewResult].

pathCost(Path, Custo) :-
    auxPathCost(Path, SumList),
    list_sum(SumList, Custo).


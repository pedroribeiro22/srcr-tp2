# Desenvolvimento

## Parser {#sec:parser}

O _Parser_ é uma aplicação em _Java_ que tem uma classe de dados, a classe
'Paragem'. Esta classe contém a informação necessária à criação de um predicado
`paragem/11` que, posteriormente, será introduzido na base de conhecimento.

A classe 'Paragem' tem as seguintes variáveis de instância:

```java
public class Paragem {

    private int gid;
    private float latitude;
    private float longitude;
    private String estadoConservacao;
    private String tipoAbrigo;
    private String publicidade;
    private String operadora;
    private ArrayList<Integer> carreira;
    private int codigoRua;
    private String nomeRua;
    private String freguesia;

}
```

Assim, por cada linha lida do ficheiro `resources/paragem.csv` é criado um
objeto do tipo `Paragem` e armazenado num `HashMap<Integer, Paragem>` que é
mapeado pelo `gid` da paragem.

Aquando da leitura do ficheiro `resources/adjacencias.csv` para cada par de
linhas que contém paragens associadas à mesma carreira é criado um predicado
`adjacencia/4`, como o seguinte exemplo:

Exemplo do conteúdo do ficheiro `resources/adjacencias.csv`:

```csv
183;-103678.36;-96590.26;Bom;Fechado dos Lados;Yes;Vimeca;01;286;Rua Aquilino
Ribeiro;Carnaxide e Queijas
791;-103705.46;-96673.6;Bom;Aberto dos Lados;Yes;Vimeca;01;286;Rua Aquilino
Ribeiro;Carnaxide e Queijas
```

Considerando um valor _distancia_ representativo da mesma, é gerado o
seguinte facto para a base de conhecimento:

`adjacencia(183, 791, distancia, 01).`

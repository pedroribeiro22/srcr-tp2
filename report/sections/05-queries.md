## Implementação das _Queries_

### Query 1

Calcular o trajeto entre 2 pontos:

Para calcular o trajeto entre 2 pontos qualquer um dos algoritmos de pesquisa
pode ser utilizado uma vez que todos os algoritmos fornecem como resultado um
caminho que conecta as paragens fornecidas como input.

### Query 2

Selecionar apenas algumas das operadoras de transporte para um determinado
percurso:

De novo, todos os algoritmos de pesquisa podem ser utilizados sendo que
passamos ao algoritmo de pesquisa um predicado e um argumento que permitem
testar se as adjacências a cada paragem constituem uma paragem que é operada
pelos operadores permitidos.

### Query 3

Excluir um ou mais operadoras de transporte para o percurso:

De novo, todos os algoritmos de pesquisa podem ser utilizados sendo que
passamos ao algoritmo de pesquisa um predicado e um argumento que permitem
testar se as adjacências a cada paragem não constituem uma paragem que é
operada pelos operadores passados como argumento.

### Query 4

Identificar quais as paragens com o maior número de carreiras num determinado
percurso:

Novamente este objetivo não requer um algoritmo de pesquisa específico. Para a
resolução deste requisito basta encontrar um caminho entre os dois pontos de
input (com qualquer um dos algoritmos). Seguidamente, calculamos o número de
carreiras associado a cada paragem. Posteriormente, encontramos o máximo dos
números calculados e recolhemos as paragens que têm esse número de carreiras,
apresentando-as ao usuário.

### Query 5

Escolher o menor percurso (usando critério menor número de paragens):

Este requisito é resolvido utilizando a estratégia de pesquisa em largura
(Breadth First). Apenas fornecemos ao algoritmo as paragens origem e destino.

### Query 6

Escolher o percurso mais rápido (usando critério da distância):

Este requisito pode ser resolvido utilizando dois algoritmos: _greedy_ ou _A*_.
Tipicamente obtêm-se melhores resultados utilizando o algoritmo A* uma vez que
este é guiado pelo custo até à próxima paragem e por uma estimativa para o
deslocamento desde a paragem atual até à paragem destino, enquanto que o
algoritmo _greedy_ tem em conta apenas o custo até à próxima paragem. Sendo
assim, o algoritmo _greedy_ é melhor para otimizações locais.

### Query 7

Escolher o percurso que passe apenas por abrigos com publicidade:

Mais uma vez, qualquer um dos algoritmos de pesquisa pode ser utilizado sendo
que é passado, como argumento, um predicado que testa se cada paragem adjacente
é tem publicidade e se, por isso, pode ser considerado como uma transição
possível.

### Query 8

Escolher o percurso que passe apenas por paragens abrigadas:

Mais uma vez, qualquer um dos algoritmos de pesquisa pode ser utilizado sendo
que é passado, como argumento, um predicado que testa se cada paragem adjacente
é coberta e se, por isso, pode ser considerado como uma transição possível.


### Query 9

Escolher um ou mais pontos intermédios por onde o percurso deverá passar:

O input neste requisitos é uma lista de paragens pelas quais o percurso deverá
passar. Assim a resolução deste requisito torna-se trivial: basta, para cada
par, aplicar um dos algoritmos de pesquisa, descobrindo, assim, um caminho que
as liga.  Ao longo da resolução do requisito vamos concatenando os
mini percursos obtidos ao resultado. No final temos um percurso que passa por
todas as paragens especificadas na lista input.


## Análise de Performance {#sec:performance_analysis}

Na seguinte tabela apresentam-se os tempos de execução das diversas _queries_
para os diferentes algoritmos, quer de pesquisa informada, quer de pesquisa
não informada.

Os tempos de execução das _queries_ são apresentados em milissegundos.

| Queries | Depth-First | Breadth-First | Greedy Search | A* Search |
|---------|-------------|---------------|---------------|-----------|
| Query 1 | 20          | 0             | 20            | 50        |
| Query 2 | 0           | 0             | 0             | 0         |
| Query 3 | 0           | 0             | 0             | 0         |
| Query 4 | 20          | 0             | 10            | 50        |
| Query 5 | -           | 0             | -             | -         |
| Query 6 | -           | -             | -             | 50        |
| Query 7 | 0           | 0             | 0             | 0         |
| Query 8 | 0           | 0             | 0             | 0         |
| Query 9 | 10          | 20            | 250           | 100       |


Casos de teste:


| Queries | Input                                 |
|---------|---------------------------------------|
| Query 1 | (183, 224)                            |
| Query 2 | (183, 595, ['Vimeca'])                |
| Query 3 | (795, 989, ['Vimeca'])                |
| Query 4 | (183, 224)                            |
| Query 5 | (183, 224)                            |
| Query 6 | (183, 224)                            |
| Query 7 | (282, 379)                            |
| Query 8 | (282, 294)                            |
| Query 9 | ([183, 499, 628, 49, 485, 172])       |

As diferenças de performance são coniventes com os princípios que aprendemos ao
longo do semestre. É normal que os algoritmos de pesquisa apresentem tempos de
execução tendencialmente superiores porque implicam comparações entre custos de
transições assim e ordenações das listas de vértices potencialmente exploráveis.
No entanto, nos algoritmos de pesquisa informada pode contar-se, de uma forma
generalizada, melhores resultados em termos de custo total do caminho, uma vez
que a exploração de vértices é feita tendo em conta o custo das transições,
conforme foi já referido.


# Testes

Uma vez que temos um _dataset_ bastante extenso não seria exequível, durante o
desenvolvimento dos algoritmos de pesquisa, testar os mesmos utilizando o
conjunto de dados que nos foi fornecido. Para o teste da correção dos
algoritmos utilizei o grafo que passo a apresentar:

![Grafo utilizado para testes](figures/test_graph.png)

Os testes desenvolvidos foram essencialmente empíricos, isto é: idealizar qual
seria o resultado para uma determinado Origem e um determinado Destino e
verificar se o _output_ do algoritmo seria esse.

Consideremos um caso de exemplo: queremos avaliar a correção do algoritmo de
procura informada _Greedy_, utilizando, para isso a Origem como 0 e o Destino
como 7.

Conforme sabemos o algoritmo em questão tem apenas em conta o custo da
transição do presente nodo para o próximo pelo que é relativamente fácil
descobrir uma solução de forma empírica. Ora vejamos: começamos no nodo 0 e
escolheremos como seguinte o nodo para o qual a transição tenha o menor custo,
no caso vertente escolhemos 1 que tem um custo de 1, em oposicão a 2 e 3 que
tem um custo de 2 e 5 respetivamente. De 1 podemos transitar para 4 ou 5, no
entanto a transição para 4 é mais barata ($4 < 9$), pelo que 4 é o próximo
nodo. De 4 é possível transitar para 8 ou 9, sendo que é mais barato ir para 8
($3 < 4$), pelo que escolhemos 8. A partir de 8 apenas podemos ir para 15, pelo
que alcançamos o nosso destino. Deste modo é expectável que o algoritmo de
pesquisa informada _Greedy_ sem restrições retorne, no presente caso, `[0, 1,
4, 8, 15]`.

De seguida verificamos esta hipótese:

```
| ?- query1greedy(0, 15).


[0,1,4,8,15]

Cost: 14 time units.

This query took [0] milliseconds.

yes
```

Verificamos a hipótese colocada pelo que validamos a implementação do algoritmo.
O mesmo processo foi seguido para os restantes algoritmos de procura.



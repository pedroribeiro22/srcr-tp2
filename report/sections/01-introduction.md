# Introdução {#sec:introduction}

O objetivo deste trabalho é a aplicação prática dos conhecimentos adquiridos,
ao longo do semestre, relativamente a algoritmos de pesquisa, quer informada,
quer não informada. Para isso foi fornecido um _dataset_ (sob a forma de um
ficheiro _CSV_) para o qual foi aconselhado o desenvolvimento de um mecanismo
que permita espelhar o conhecimento que nele consta numa base de conhecimento
em _PROLOG_.

Deste modo, ao longo deste relatório apresentarei uma explicação do projeto a
que chamei de "Parser", que é o responsável pela conversão dos dados do _CSV_
para uma base de conhecimento em PROLOG. Posteriormente, apresentarei uma breve
explicação da implementação dos algoritmos de pesquisa e do raciocínio
subjacente á implementação das respostas aos objetivos do trabalho (que
designei de "Queries"). Finalizarei com uma análise comparativa dos algoritmos
para os diferentes cenários pedidos.

\newpage


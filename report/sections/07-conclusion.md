# Conclusão {#sec:conclusion}

Os objetivos deste trabalho individual foram cumpridos uma vez que é possível
obter respostas a todos os cenários enumerados no enunciado. Foi encontrada uma
estratégia válida para transformar o conhecimento que consta dos ficheiros CSV
numa base de conhecimento em PROLOG.

